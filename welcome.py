import pygame
from line import GameLogic
from text import DisplayText
from GameLoop import GameLoop

class Welcome:
    def __init__(self):
        self.BLUE = (0, 0, 255)
        self.backgroung_color = (255, 255, 204)

    def display_welcome(self, screen):
        play = False
        clock = pygame.time.Clock()
        display_text = DisplayText()
        game_play = GameLoop()
        while not play:
            screen.fill(self.backgroung_color)
            display_text.render_text(screen, "Welcome to Tic-Toc-Toe", self.BLUE, 80, 50)
            display_text.render_text(screen, "Press key s to play", self.BLUE, 110, 80)
            display_text.render_text(screen, "presh space to reset", self.BLUE, 100, 110)
            display_text.render_text(screen, "player-1 symbol is X", self.BLUE, 100, 140)
            display_text.render_text(screen, "player-2 symbol is O", self.BLUE, 100, 170)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    play = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_s:
                        game_play.game_loop(screen)
            pygame.display.update()
            clock.tick(60)