import pygame
from line import GameLogic
from text import DisplayText
from tkinter import *
from tkinter import messagebox

class GameLoop:
    def __init__(self):
        self.crashed = False
        self.backgroung_color = (255, 255, 204)
        self.line = pygame.Rect(0, 500, 500, 8)

    def game_loop(self, screen):
        pygame.mixer.init()
        pygame.init()
        player = 'x'
        game_logic = GameLogic()
        display_text = DisplayText()
        clock = pygame.time.Clock()
        while not self.crashed:
            if game_logic.count_win == 3 and game_logic.game_finish and game_logic.player_win == 'x':
                Tk().wm_withdraw()
                messagebox.showinfo("Result", 'X win!')
                game_logic.clear_game()
                game_logic.game_finish = False
            elif game_logic.count_win == 3 and game_logic.game_finish and game_logic.player_win == 'o':
                Tk().wm_withdraw()
                messagebox.showinfo("Result", "O win!")
                game_logic.clear_game()
                game_logic.game_finish = False
            if game_logic.is_tie():
                Tk().wm_withdraw()
                messagebox.showinfo("Result", "Match tie")
                game_logic.clear_game()
                game_logic.game_finish = False
                    
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.crashed = True
                if event.type == pygame.MOUSEBUTTONUP and not game_logic.game_finish:
                    if pygame.mouse.get_pressed():
                        pygame.mixer.music.load("beep.mp3")
                        place = pygame.mouse.get_pos()
                        game_logic.get_hover(screen, place[0]//167, place[1]//167, player)
                        if game_logic.change_player:
                            if player == 'x':
                                player = 'o'
                            elif player == 'o':
                                player = 'x'
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE and game_logic.game_finish:
                        game_logic.clear_game()
                        game_logic.game_finish = False
                    elif event.key == pygame.K_SPACE:
                        self.crashed = False
            screen.fill(self.backgroung_color)
            display_text.render_text(screen, "Player-1", (0, 0, 0), 0, 540)
            display_text.render_text(screen, str(game_logic.score_one)+'-'+str(game_logic.score_two), (0, 0, 0), 230, 540)
            display_text.render_text(screen, "Player-2", (0, 0, 0), 375, 540)
            game_logic.draw_line_for_grid(screen)
            pygame.draw.rect(screen, (0, 0, 0), self.line)
            pygame.display.update()
        clock.tick(60)
        pygame.quit()
        quit()