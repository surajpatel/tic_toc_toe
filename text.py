import pygame
# from line import GameLogic

class DisplayText:
    def __init__(self):
        self.font = pygame.font.SysFont(None, 45)

    def render_text(self, screen, text, color, left, top):
        
        display_text = self.font.render(text, True, color)
        screen.blit(display_text, [left, top])
