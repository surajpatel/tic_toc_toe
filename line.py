import pygame
import os
from text import DisplayText
from tkinter import *
from tkinter import messagebox

player_x = pygame.image.load(os.path.join('images', 'xx.jpg'))
player_o = pygame.image.load(os.path.join('images', 'oo.jpg'))
class GameLogic:
    def __init__(self):
        self.line_draw_for_grid = [((0,167),(500,167)),((0,334),(500,334)),((167,0),(167,500)),((334,0),(334, 500))]
        self.no_of_line = [[0 for x in range(3)] for y in range(3)]
        self.change_player = True
        self.search_direction = [(0,-1),(-1,-1),(-1,0),(-1,1),(0,1),(1,1),(1,0),(1,-1)]
        self.game_finish = False
        self.score_one = 0
        self.score_two = 0
        self.count_win = 0
        self.player_win = ''

    def draw_line_for_grid(self, screen):
        for line_display in self.line_draw_for_grid:
            pygame.draw.line(screen, (234, 68, 56), line_display[0], line_display[1], 2)
        for y_axis in range(len(self.no_of_line)):
            for x_axis in range(len(self.no_of_line[y_axis])):
                if self.get_box_value(x_axis,y_axis) == 'x':
                    screen.blit(player_x,(x_axis*167, y_axis*167))
                elif self.get_box_value(x_axis,y_axis) == 'o':
                    screen.blit(player_o,(x_axis*167, y_axis*167))

    def get_box_value(self, x_axis, y_axis):
        return self.no_of_line[y_axis][x_axis]

    def set_box_value(self, x_axis, y_axis, value):
        self.no_of_line[y_axis][x_axis] = value

    def get_hover(self,screen, x_axis, y_axis, player):
        if self.get_box_value(x_axis,y_axis) == 0:
            self.change_player = True
            if player == 'x':
                self.set_box_value(x_axis,y_axis,'x')
            elif player == 'o':
                self.set_box_value(x_axis,y_axis,'o')
            player_win = self.check_matches(screen, x_axis,y_axis,player)
            if player_win == 'x':
                self.score_one += 1
            elif player_win == 'o':
                self.score_two += 1
        else:
            self.change_player = False

    def check_bound(self,x_axis,y_axis):
        return x_axis >= 0 and x_axis<3 and y_axis>=0 and y_axis<3

    def check_matches(self,screen, x_axis,y_axis,player):
        count = 1
        for index, (direction_x, direction_y) in enumerate(self.search_direction):
            if self.check_bound(x_axis+direction_x, y_axis+direction_y) and self.get_box_value(x_axis+direction_x, y_axis+direction_y) == player:
                count += 1
                x_axis_sum = x_axis + direction_x
                y_axis_sum = y_axis +direction_y
                if self.check_bound(x_axis_sum+direction_x, y_axis_sum+direction_y) and self.get_box_value(x_axis_sum+direction_x, y_axis_sum+direction_y) == player:
                    count += 1
                    if count == 3:
                        break
                if count < 3:
                    new_direction = 0
                    if index == 0:
                        new_direction = self.search_direction[4]
                    elif index == 1:
                        new_direction = self.search_direction[5]
                    elif index == 2:
                        new_direction = self.search_direction[6]
                    elif index == 3:
                        new_direction = self.search_direction[7]
                    elif index == 4:
                        new_direction = self.search_direction[0]
                    elif index == 5:
                        new_direction = self.search_direction[1]
                    elif index == 6:
                        new_direction = self.search_direction[2]
                    elif index == 7:
                        new_direction = self.search_direction[3]
                    
                    if self.check_bound(x_axis +new_direction[0], y_axis+new_direction[1]) and self.get_box_value(x_axis +new_direction[0], y_axis+new_direction[1]) == player:
                        count += 1
                        if count == 3:
                            break
                    else:
                        count = 1
        if count == 3:
            if player == 'x':
                self.count_win = 3
                self.player_win = 'x'
            elif player == 'o':
                self.count_win = 3
                self.player_win = 'o'

            self.game_finish = True
            return player
        else:
            if self.is_tie() and count < 3:
                self.count_win = 0
                self.player_win = 'tie'
            self.game_finish = self.is_tie()
    
    def is_tie(self):
        for row in self.no_of_line:
            for value in row:
                if value == 0:
                    return False
        return True

    def clear_game(self):
        for y in range(len(self.no_of_line)):
            for x in range(len(self.no_of_line[y])):
                self.set_box_value(x,y,0)
