'''Tic toc toe game'''
import pygame
from welcome import Welcome

def play_game():
    pygame.init()

    display_width = 500
    display_height = 600
    screen = pygame.display.set_mode((display_width, display_height))
    pygame.display.set_caption('Tic Toc Toe')

    welcome_display = Welcome()
    welcome_display.display_welcome(screen)

play_game()
